
CREATE DATABASE blog_db;

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    pword VARCHAR(300) NOT NULL,
    date_created DATETIME NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    author_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_user_id
        FOREIGN KEY(author_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE post_likes (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_post_post_likes_id
        FOREIGN KEY(post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_user_post_likes_id
        FOREIGN KEY(user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

CREATE TABLE post_comments (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_posts_post_comments_id
        FOREIGN KEY(post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_user_post_comments_id
        FOREIGN KEY(user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

-- Add the following records to the blog_db database:
INSERT INTO users(
    email, 
    pword, 
    date_created) 
    VALUES 
        (
        'johnsmith@gmail.com', 
        'passwordA', 
        '2021-01-01 01:00:00'
        );

INSERT INTO users(
    email, 
    pword, 
    date_created) 
    VALUES 
        (
            'juandelacruz@gmail.com', 
            'passwordB', 
            '2021-01-01 02:00:00'
        ),
        (
            'janesmith@gmail.com', 
            'passwordC', 
            '2021-01-01 03:00:00'),
        (
            'mariadelacruz@gmail.com',
            'passwordD', 
            '2021-01-01 04:00:00'
        ),
        (
            'johndoe@gmail.com', 
            'passwordE', 
            '2021-01-01 05:00:00'
        );

-- POSTS
INSERT INTO posts(
    author_id, 
    title, 
    content,
    datetime_posted) 
    VALUES 
        (
            '1',
            'First Code',
            'Hello World!',
            '2021-01-02 01:00:00'
        ),
        (
            '1',
            'Second Code',
            'Hello Earth!',
            '2021-01-02 02:00:00'
        ),
        (
            '2',
            'Third Code',
            'Welcome to Mars!',
            '2021-01-02 03:00:00'
        ),
        (
            '4',
            'Fourth Code',
            'Bye bye solar system!',
            '2021-01-02 04:00:00'
        );

-- Get all the title with an user ID of 1.
SELECT title 
FROM posts 
WHERE author_id = "1";

-- Get all the user's email and datetime of creation.
SELECT email, date_created
FROM users;

-- Update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth" by using the record's ID.
UPDATE posts
SET content = 'Hello to the people of the Earth!'
WHERE content = 'Hello Earth!' AND author_id = '1'

-- Delete the user with an email of "johndoe@gmail.com".

DELETE FROM users
WHERE email = "johndoe@gmail.com"